# README #

Deploy and update BioViz.org content and applications using Ansible.

* Create bioviz_vars.yml (see `example_vars.yml`)
* Place certificates in `roles/apache/files`
* Run `ansible-playbook setup.yml`

### Who do I talk to? ###

* Ann Loraine aloraine@uncc.edu
